resource "aws_subnet" "subnet-uno" {
  cidr_block = "${cidrsubnet(aws_vpc.currency-exchange.cidr_block, 3, 1)}"
  vpc_id = "${aws_vpc.currency-exchange.id}"
  availability_zone = "ap-southeast-2a"
}

resource "aws_route_table" "route-table-currency-exchange" {
  vpc_id = "${aws_vpc.currency-exchange.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.currency-exchange-gw.id}"
  }

  tags = {
    Name = "currency-exchange-route-table"
  }
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id = "${aws_subnet.subnet-uno.id}"
  route_table_id = "${aws_route_table.route-table-currency-exchange.id}"
}