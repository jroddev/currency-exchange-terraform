resource "aws_internet_gateway" "currency-exchange-gw" {
  vpc_id = "${aws_vpc.currency-exchange.id}"

  tags = {
    Name ="currency-exchange-gw"
  }
}