resource "aws_instance" "currency-exchange-ec2-instance" {
  ami = "${var.ami_id}"
  instance_type = "t2.micro"
  key_name = "${var.ami_key_pair_name}"
  vpc_security_group_ids = ["${aws_security_group.ingress-all-currency-exchange.id}"]
  subnet_id = "${aws_subnet.subnet-uno.id}"
  user_data = "${file("user_data.sh")}"

  tags = {
    Name = "${var.instance_name}"
  }
}