# Based on 
https://medium.com/@hmalgewatta/setting-up-an-aws-ec2-instance-with-ssh-access-using-terraform-c336c812322f

# Some more details 
https://medium.com/@madeeshafernando/why-terraform-9290a82b104e

# Setup AWS Commandline
❯ aws configure
AWS Access Key ID [None]: XXXXXXXXXXXXXXXXX
AWS Secret Access Key [None]: /XXXXXXXXXXXXXXXXXXXXXXXXXXXX
Default region name [None]: ap-southeast-2
Default output format [None]:

# Terraform Create Infrastructure
terraform init
terraform plan -var-file=secret.tfvars
terraform apply -var-file=secret.tfvars -var-file=production.tfvars

# Terraform Delete Infrastructure
terraform destroy -var-file=secret.tfvars -var-file=production.tfvars
