resource "aws_vpc" "currency-exchange" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "currency-exchange"
  }
}


resource "aws_eip" "ip-currency-exchange" {
  instance = "${aws_instance.currency-exchange-ec2-instance.id}"
  vpc = true
}

resource "aws_route53_record" "currency-exchange-route53-record" {
  zone_id = "${var.route53_zone_id}"
  name    = "currency-exchange.jroddev.com"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.ip-currency-exchange.public_ip}"]
}

